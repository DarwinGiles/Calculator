var operators = ['+', '-', '*', '/']
var bottomRow = ['.', '0', 'clear']

var controlPanel = document.querySelector("#control-panel")
for (y = 0; y < 4; y++)
{
    for (x = 0; x < 4; x++)
    {
        var button = document.createElement('button');
        controlPanel.appendChild(button);

        if (x == 3)
        {
            button.textContent = operators[y];
            button.classList.add('operator-button');
            button.addEventListener('click', onDisplayKeyPressed);
        }
        else if (y == 3)
        {
            button.textContent = bottomRow[x];

            switch (x)
            {
                case 0:
                button.addEventListener('click', onDisplayKeyPressed);
                break;
                case 1:
                button.addEventListener('click', onDisplayKeyPressed);
                break;
                case 2:
                button.addEventListener('click', onClearKeyPressed);
                break;
            }
        }
        else
        {
            button.textContent = (y * 3) + x + 1;
            button.addEventListener('click', onDisplayKeyPressed)
        }
    }
}

var equalsButton = document.createElement('button');
controlPanel.appendChild(equalsButton);
equalsButton.id = 'equals-button'
equalsButton.textContent = '=';
equalsButton.addEventListener('click', onEqualsKeyPressed);

var displayText = "";
var display = document.querySelector('#screen');
function onDisplayKeyPressed(e)
{
    displayText += this.textContent;
    display.textContent = displayText;
}

function onClearKeyPressed(e)
{
    displayText = '';
    display.textContent = displayText;
}

function onEqualsKeyPressed(e)
{
    displayText = parseEquation(displayText);
    display.textContent = displayText;
}

function parseEquation(str)
{
    if (str.includes('*'))
    {
        var split = str.split('*');
        var result = 1;
        split.forEach(element => {
            result *= parseEquation(element)
        });
        return result;
    }
    else if (str.includes('/'))
    {
        var split = str.split('/');
        var result = 1;
        split.forEach(element => {
            result /= parseEquation(element)
        });
        return result;
    }
    else if (str.includes('+'))
    {
        var split = str.split('+');
        var result = 1;
        split.forEach(element => {
            result += parseEquation(element)
        });
        return result;
    }
    else if (str.includes ('-'))
    {
        var split = str.split('-');
        var result = 1;
        split.forEach(element => {
            result -= parseEquation(element)
        });
        return result;
    }
    else
    {
        return +str
    }
}
